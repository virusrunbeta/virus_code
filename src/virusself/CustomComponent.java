package virusself;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.net.URL;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;

/**
 * Formating of the JLabels and JButtons
 */
public class CustomComponent {
	
	public static void customJLabel(JComponent label, Color color, int x, int y, int width, int height, int police) {
		label.setBounds(x, y, width, height);
		label.setForeground(color);
		label.setFont(new Font("System", Font.BOLD, police));
	}
	
	public static void customJButton(JButton button, Color color, int x, int y, int width, int height, int police) {
		button.setBackground(color);
		button.setOpaque(true);
		button.setBorderPainted(false);
		button.setBounds(x, y, width, height);
		button.setForeground(Color.black);
		button.setFont(new Font("System", Font.BOLD, police));
	}
	
	//only for the Homescreen
	public static Image customImage(URL url, int width, int height) {
		Image image = new ImageIcon(url).getImage();
		Image resized = image.getScaledInstance(width, height, Image.SCALE_DEFAULT);
		Image picture = new ImageIcon(resized).getImage(); 
		return picture;
		
	}
}


