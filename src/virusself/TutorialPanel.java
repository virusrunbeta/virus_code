package virusself;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;


@SuppressWarnings("serial")
public class TutorialPanel extends JPanel {

	private Tools toolbox = new Tools();
	private JLabel instructionVirus, instructionGrannies, instructionDoctor, instructionTrampo, instructionVaccine;
	
	public TutorialPanel(Interface mainPanel) {
		
		this.setLayout(null);
		this.setBackground(toolbox.aliceBlue);

		instructionVirus = new JLabel("Here you are, a little virus. Your goal is to infect as much people as possible.");
		CustomComponent.customJLabel(instructionVirus, Color.black, 605, 100, 600, 30, 14);
		add(instructionVirus);

		instructionGrannies = new JLabel("These are grannies, your main pray. Infecting them gives you additional lifetime.");
		CustomComponent.customJLabel(instructionGrannies, Color.black, 605, 200, 600, 30, 14);
		add(instructionGrannies);

		instructionDoctor = new JLabel("Beware of the villain doctors. They reduce your life by one.");
		CustomComponent.customJLabel(instructionDoctor, Color.black, 605, 300, 600, 30, 14);
		add(instructionDoctor);
		

		instructionTrampo = new JLabel("<html><p>Hop on trampolines to get to another continent. But beware,<br/>  some may take you back to places you've already been ... </p></html>");
		CustomComponent.customJLabel(instructionTrampo, Color.black, 605, 390, 600, 40, 14);
		add(instructionTrampo);
	
		
		instructionVaccine = new JLabel("Collecting these vaccins gives you a chance of regaining a heart");
        CustomComponent.customJLabel(instructionVaccine, Color.black, 605, 495, 600, 30, 14);
        this.add(instructionVaccine);
		
		JButton exit = new JButton("Got it!");
		CustomComponent.customJButton(exit, new Color(160, 160, 160), 575, 600, 150, 50, 14);
		add(exit);

		exit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				mainPanel.switchPanel(mainPanel.homeScreen);
			}
		});
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		g.drawImage(CustomComponent.customImage(getClass().getResource("/images/virus.png"),100,100), 400, 60, null);
		g.drawImage(CustomComponent.customImage(getClass().getResource("/images/grandma.png"),100,100), 400, 170, null);
		g.drawImage(CustomComponent.customImage(getClass().getResource("/images/doctor.png"),100,100), 400, 270, null);
		g.drawImage(CustomComponent.customImage(getClass().getResource("/images/trampoline.png"),100,100), 400, 360, null);
		g.drawImage(CustomComponent.customImage(getClass().getResource("/images/vaccine.png"),100,100), 400, 440, null);
	}
}

