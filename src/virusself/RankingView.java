
package virusself;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import gm.Score;

import java.util.List;


public class RankingView {

	private JLabel table; 
	private Image classement;

	public RankingView() {
		classement = new ImageIcon(CustomComponent.customImage(RankingView.class.getResource("/images/ranking.png"),638,640)).getImage();
		table = new JLabel("RANKING");
		CustomComponent.customJLabel(table, Color.BLACK, 110,230,490,500,20);
	}

	public void showRanking(List<Score> topSix) {
		
		JFrame rframe = new JFrame();
		rframe.setPreferredSize(new Dimension(696,800));
		rframe.setResizable(false);
		rframe.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		rframe.pack();
		rframe.setVisible(true);
		JPanel rankingPanel = new JPanel() {
			@Override
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				g.drawImage(classement,50,60,null);
			}
		};
		rankingPanel.setBackground(new Color(255,77,77));
		rankingPanel.setLayout(null);
		rankingPanel.add(table);
		rframe.add(rankingPanel);
		String highScore = "<html><h1> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;RANKING</h1>"
				+ "<tr>"
				+ "<th>Player</th>"
				+ "<th> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Score</th>"
				+ "</tr>";

		for (Score s: topSix) {
			highScore += "<tr><td>" + s.player + "</td><td> &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;"+ s.score+ "</td> </tr>";
		}
		highScore += "</html>";
		table.setText(highScore);
	}
}