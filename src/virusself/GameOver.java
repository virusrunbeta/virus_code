package virusself;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

//import gm.Score;

@SuppressWarnings("serial")
public class GameOver extends JPanel implements ActionListener{

	private Tools toolbox = new Tools();
	
	private JButton replayButton, otherDifficultyButton, confirmNameButton;
	private JLabel nickNameLabel, scoreLabel, sendMessage;
	private JTextArea namePlayer;
	
	public GameOver(Interface mainPanel, int score, int currentDifficulty) { //when we replay, we want a board with the same difficulty (currebtDifficulty) as the last game we played
		

		this.setLayout(null);
		this.setBackground(new Color(255,77,77));
		
		nickNameLabel = new JLabel("NICKNAME : ");
		CustomComponent.customJLabel(nickNameLabel, toolbox.aliceBlue, 488, 565, 150, 30, 18);
		add(nickNameLabel);
		
		namePlayer = new JTextArea();
		CustomComponent.customJLabel(namePlayer, Color.black, 608, 565, 100, 30, 18);
		namePlayer.setBackground(toolbox.aliceBlue);
		namePlayer.setOpaque(true);
		add(namePlayer);
		

		sendMessage = new JLabel(" SENT");
		CustomComponent.customJLabel(sendMessage, new Color(59,122,87), 730, 565, 60, 30, 18);
		sendMessage.setBackground(toolbox.aliceBlue);
		sendMessage.setOpaque(true);
		sendMessage.setVisible(false);
		add(sendMessage);

	
		replayButton = new JButton("REPLAY");
		CustomComponent.customJButton(replayButton,toolbox.aliceBlue,90,500,150,50,18);
		add(replayButton);
		replayButton.addActionListener(new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			mainPanel.gameWindow.gamePlanMaker(mainPanel, currentDifficulty);
			mainPanel.gameTimer.start();
				
			}
		});

		scoreLabel = new JLabel("SCORE : " + score);
		CustomComponent.customJLabel(scoreLabel, Color.BLACK, 562, 430, 200, 50, 30);
		add(scoreLabel);
		
	
		confirmNameButton = new JButton("CONFIRM");
		CustomComponent.customJButton(confirmNameButton, Color.WHITE,713,570,110,20,12);
		add(confirmNameButton);
		confirmNameButton.addActionListener(new ActionListener() {	
		@Override
		public void actionPerformed(ActionEvent e) {
			String player = namePlayer.getText();				
			Ranking ranking = new Ranking(score, player);
			RankingView view = new RankingView();
			view.showRanking(ranking.getTopSix());
			
			sendMessage.setVisible(true);
			confirmNameButton.setVisible(false);
			}
		});
		
		
		otherDifficultyButton = new JButton("<html><p>TRY ANOTHER<br/>  DIFFICULTY </p></html>");
		CustomComponent.customJButton(otherDifficultyButton,toolbox.aliceBlue,1050,500,200,50,18);
		add(otherDifficultyButton);
		otherDifficultyButton.addActionListener(new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			mainPanel.switchPanel(mainPanel.homeScreen, mainPanel.music1);
			}
		});	
		
	}

	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Image gameover = new ImageIcon(getClass().getResource("/images/gameover.png")).getImage();
		Image resized1 = gameover.getScaledInstance(1280, 700, Image.SCALE_DEFAULT);
		Image gameOver = new ImageIcon(resized1).getImage();

		g.drawImage(gameOver, 0, 7, null);

	}
	

	@Override
	public void actionPerformed(ActionEvent e) {	
	}
}
	
