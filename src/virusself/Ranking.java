package virusself;

import java.util.ArrayList;
import java.util.List;

import gm.ApiBoard;
import gm.Score;


public class Ranking {
	
	List<Score> ranking, topSix;
	
	/**
	 * Creates a list of scores with player's name (input GameOver.namePlayer) and points
	 *
	 */
	public Ranking(int point, String player) {
		
		String success_message = ApiBoard.addResult((float)point, player, "VirusRun");
		ranking = ApiBoard.getResults("VirusRun");
		topSix = new ArrayList<Score>();
		topSix.add(0, ranking.get(0));
		ranking.remove(0);
		if (ranking.size()>6){
			for(int indexTopSix = 1; indexTopSix < 7; indexTopSix++) {
				Score max = ranking.get(0);
				int maxPosition = 0;
				for(int i = 0; i < ranking.size(); i++) {
					int currentScore = Integer.valueOf(ranking.get(i).score.substring(0,(ranking.get(i).score.indexOf('p') - 1)));
					int maxScore = Integer.valueOf(max.score.substring(0,(max.score.indexOf('p') - 1)));
					if (currentScore > maxScore) {
						max = ranking.get(i);
						maxPosition = i;
						}
				}
				ranking.remove(maxPosition);
				topSix.add(indexTopSix, max);
			}
		}
	}
	public List<Score> getRanking() {
		return ranking;
	}
	public List<Score> getTopSix() {
		return topSix;
	}
}
