package virusself;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.ImageObserver;


public class SpecialTile extends Tile implements ImageObserver {


	public int valueGrannys;

	public SpecialTile(int x, int y, int value) { //constructor used for the grandmothers
		super(x,y);
		valueGrannys = value;
	}
	
	public SpecialTile(int x, int y) {
		super(x, y);
	}

	
	public void draw(Graphics g, Image image) {
		g.drawImage(image,(int)(coord[0]*RIB + 2*PLAATS)  ,(int)(coord[1]*RIB + 2*PLAATS), (ImageObserver) this);
	}

	
	@Override
	public boolean imageUpdate(Image img, int infoflags, int x, int y, int width, int height) {
		// TODO Auto-generated method stub
		return false;
	}
	 

}
