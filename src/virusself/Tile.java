package virusself;

import java.awt.Color;
import java.awt.Graphics;

public class Tile {

	public Color tileColor;
	public int[] coord = new int[2]; //coordinates of the Tile
	public int RIB; 
	public static final int PLAATS = 1; // space between each Tile
	boolean bezet = false;	//used to have only one specialTile on a Tile
	public boolean isVisible = true;
	
	public Tile(int x, int y) {															
		coord[0] = x;
		coord[1] = y;
	}

	public void draw(Graphics g, int RIB) {
		g.setColor(tileColor);
		g.fillRect(PLAATS + coord[0] * RIB, PLAATS + coord[1] * RIB, RIB - 2 * PLAATS, RIB - 2 * PLAATS);
	}

}