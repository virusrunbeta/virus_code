package virusself;

import java.awt.Color;
import java.util.Arrays;
import java.util.Random;

public class Tools {
	
	
	public int rng(int max) { 			// random number generator
		Random rng = new Random();
		return rng.nextInt(max);
	}

	/**
	 * Gives a random number between low and high
	 * @param low
	 * @param high
	 * 
	 */
	public int rndg(int low, int high) { // genereert een random getal tussen low en high
		return rng(high - low) + low;
	}

	public Color kleuren(int level) { 			// genereert een kleur voor elk level
		if (level == 0) {
			return new Color(0, rndg(0, 191), rndg(240, 255)); 				// Europe
		} else if (level == 1) {
			return new Color(255, 255, rndg(0, 204)); 						// Amériqe nord
		} else if (level == 2) {
			return new Color(0, rndg(100, 255), 0); 						// Asie
		} else if (level == 3) {
			return new Color(255, rndg(69, 215), 0); 						// Amérique sud
		} else if (level == 4) {	
			return new Color(rndg(139, 210), rndg(69, 105), rndg(19, 30));	// Afrique
		} else if (level == 5) {
			return new Color(rndg(240, 255), 255, rndg(240, 255)); 			// Antartique
		} else if (level == 6) {
			return new Color(rndg(128, 180), rndg(0, 42), rndg(0, 60)); 	// Oceanie
		} else
			return null;	
	}


	public Color cherryPink = new Color(255,179,204);
	public Color paleBlue = new Color(179,255,229);
	public Color lightCoral = new Color(240,128,128);
	public Color aliceBlue = new Color(237,238,255);
	
	
	/**
	 * generate a random coordinate with from 0 (inclusive) to x and y (exclusive)
	 * @param x
	 * @param y
	 * @return
	 */
	
	public int[] randomCoord(int x, int y) {
		int[] destination = new int[2];
		destination[0] = this.rng(x);
		destination[1] = this.rng(y);
		return destination;
		}
	

}
