package virusself;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.*;

@SuppressWarnings("serial")
public class Interface extends JPanel {
	
	private JPanel activePanel; 	
	public Timer gameTimer;
	
	static final int framewidth = 1280;
	static final int frameheight = 800;
	
	public HomeScreen homeScreen;
	public TutorialPanel tutorialPanel;
	public GameWindow gameWindow; 


	private Sounds sounds = new Sounds();
	public String music1 = "/musics/song2.wav" ;
	public String music2 = "/musics/version1.wav";


	public Interface(){
		this.homeScreen = new HomeScreen(this);//, sounds);
		this.tutorialPanel = new TutorialPanel(this);
		this.gameWindow = new GameWindow(this);
		
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS)); // deze constructor werd geinspireerd door IR wars 2017)
		activePanel = new HomeScreen(this);//, sounds);//dit is het eerste
		
		add(activePanel);
		validate();
		repaint();
	}	
	
	//Het spel

	public static void main(String[] args) {

	 JFrame f = new JFrame();
     f.setSize(Interface.framewidth, Interface.frameheight);
     f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
     f.setTitle("Virusrun");
     f.setLocation(50, 50);
 	 
     
     Interface activePanel = new Interface();
     
     f.add(activePanel,BorderLayout.CENTER);
     f.add(activePanel.gameWindow,BorderLayout.PAGE_START);
    
     activePanel.gameWindow.setPreferredSize(new Dimension(1280, 60));
     activePanel.gameWindow.setBackground(Color.BLACK);
     
     f.setResizable(false);
     f.setVisible(true);
	}

	public void switchPanel(JPanel toActivate, String music){  //deze methode  werd geinspireerd door IR wars 2017)

		this.remove(activePanel);
		activePanel = toActivate; 
		this.add(activePanel);	
		activePanel.requestFocusInWindow();
		validate();
		repaint();
		sounds.switchMusic(music);
	}
	
	public void switchPanel(JPanel toActivate){
		this.remove(activePanel);
		activePanel = toActivate; 
		this.add(activePanel);	
		activePanel.requestFocusInWindow();
		validate();
		repaint();
	}

	public void switchBoardPanel(Board toActivate){		//to change between boards

		this.remove(activePanel);
		toActivate.active = true;
		activePanel = toActivate;
		this.add(toActivate, BorderLayout.CENTER);	
		activePanel.requestFocusInWindow();
		repaint();
		validate();
	}

}

