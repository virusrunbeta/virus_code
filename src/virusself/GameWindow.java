
package virusself;

import java.awt.Color;
import java.awt.event.*;
import java.util.Arrays;

import javax.swing.*;

@SuppressWarnings("serial")
public class GameWindow extends JPanel implements ActionListener {

	private JButton playButton, pauseButton;
	private TimeBar timeBarPaneel;
	private JLabel levelLabel, scoreLabel;
	private String[] continent = {"Europe","North America","Asia", "South America","Africa","Antarctica", "Australia", "Paradise"};

	
	public  int bdw = 27, bdh = 5; 	// boardWidth, boardHeight of the first board, gets changed afterwards
	private int RIB = (int) (Interface.framewidth / (double) bdw);
	
	private Virusperso virus = new Virusperso(0,bdh/2);
	private Board[] levels = new Board[8];
	private Tools toolbox = new Tools();

	public int difficulty = 0;		//0 is easy, 1 normal, 2 hard
	public int score = 0;
	public int currentLevel = 0;

	private Interface mainPanel;
	private Timer updateTimer = new Timer(20, this);

	
	
	public GameWindow(Interface mainPanel) {
		
		for(int i = 0; i < 3; i++) {	//the little hearts in the top right corner
			this.add(virus.hearts[i]);}
		
		this.setLayout(null);

		
		this.timeBarPaneel = new TimeBar();
		this.add(timeBarPaneel);
		timeBarPaneel.setBounds(350,12,320,30);
		timeBarPaneel.setBackground(Color.black);
		
		levelLabel = new JLabel("HomeScreen");
		CustomComponent.customJLabel(levelLabel, toolbox.lightCoral, 55, 12, 300, 30, 26);
		add(levelLabel);
		
		scoreLabel = new JLabel("SCORE: 0");
		CustomComponent.customJLabel(scoreLabel, toolbox.lightCoral, 1070, 12, 300, 30, 30);
		add(scoreLabel);
		
		//Pause/play button :
		//When we click on pauseButton it stops the TimeBar and the game board and it changes the 
		//button to a playButton.
		pauseButton = new JButton("PAUSE");
        CustomComponent.customJButton(pauseButton, toolbox.aliceBlue, 700,12,90,30,14);
        this.add(pauseButton);
        pauseButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                mainPanel.gameTimer.stop();
                updateTimer.stop();
                levels[currentLevel].active = false; 
                pauseButton.setVisible(false);
                playButton.setVisible(true);
            }
        });
		
        playButton = new JButton("PLAY");
        CustomComponent.customJButton(playButton, toolbox.aliceBlue, 700,12,90,30,14);
        add(playButton);
        playButton.setVisible(false);
        playButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                mainPanel.gameTimer.start();
                updateTimer.start();
                levels[currentLevel].active = true;
                playButton.setVisible(false);
                pauseButton.setVisible(true);
            }
        });

		
		
		ActionListener time = new ActionListener() {	
		public void actionPerformed(ActionEvent evt) {
				if (timeBarPaneel.currentTime > 0) {
					timeBarPaneel.revalidate();
					timeBarPaneel.repaint();
					timeBarPaneel.changeWidth(-1);	 
				}
				if (timeBarPaneel.currentTime <= 0) {				
					mainPanel.switchPanel(new GameOver(mainPanel, score, difficulty));
					mainPanel.gameTimer.stop();
					levels[currentLevel].active = false;
					resetVirus();
				}
			}
		};
		mainPanel.gameTimer = new Timer(1000,time); //the timer changing the timerbar	
		
	}	
	
	public void gamePlanMaker(Interface mainPanel, int difficulty) {

		updateTimer.start();
		//reset of the game	
			timeBarPaneel.currentTime = 60;
			virus.life = 3;
			currentLevel = 0;
			for(int i = 0; i < levels.length; i++ ) {
				this.levels[i] = null;}	

			bdw = 9;
			bdh = 5;
			virus.coord[0] = 0;
			virus.coord[1] = bdh/2;
			updateRIB();
			resizeVirus(virus);
		//------------
	
			this.mainPanel = mainPanel;
			this.difficulty = difficulty;
			
			// (bdw, bdh, #docs, #trampos, #grans, #vaccines, masker on or of, virus, level, grannyValue, doctorSpeed)
			this.levels[0] = new Board(bdw, bdh, 0, 2, 6, 0, false, virus, 0, 2, 300);
            this.levels[1] = new Board(13, 7, 1, 3, 9, 0, false, virus, 1, 2, 700);
            this.levels[2] = new Board(16, 9, 2, 4, 20, 1, false, virus, 2, 3, 500);
            this.levels[3] = new Board(18, 10, 3, 5, 15, 1, false, virus, 3, 3, 650);
            this.levels[4] = new Board(21, 12, 4, 6, 15, 2, false, virus, 4, 3, 400);
            this.levels[5] = new Board(27, 15, 1, 7, 16, 3, false, virus, 5, 4, 220);
            this.levels[6] = new Board(32, 18, 8, 8, 33, 1, false, virus, 6, 4, 300);
            this.levels[7] = new Board(6, 3, 1, 0, 1, 0, false, virus, 5, 1000, 500);

			
			if(difficulty == 2) {	//when playing in hard the board has a black foreground
				for(int i = 0; i < levels.length; i++ ) {
					levels[i].mask = true;
				}
			}
			
			levels[0].active = true;
		
			if(difficulty == 0) {	//different music is played depending on difficulty
				mainPanel.switchPanel(levels[0], mainPanel.music1);}	
			else {
				mainPanel.switchPanel(levels[0], mainPanel.music2);}	
		}

	/**
	 * Every 20 miliseconds we check if the virus is on a trampoline, if it is the case, 
	 * we use the method change to change the currentboard to the desired level
	 * 
	 */

	public void levelChangeCheck() {	

		/*Changing levels goes like this: Every trampoline of the board gets assigned a level to which it will teleport the virus. 
		 * This level is the index of the trampoline in the trampos array.
		 * In the easy level you can only fall one level down, this is why the for loop starts from currentLevel-1 
		 * */
		
		
		if (difficulty == 0) {	//EASY LEVEL
					
				//Because the for loop starts from currentLevel -1, the level 0 had to be taken care of on its own
				if (currentLevel == 0) {
					if (Arrays.equals(levels[currentLevel].trampos[1].coord, virus.coord)) {
						switchLevel(levels[currentLevel], levels[1]);
						currentLevel = 1;	
				}}
				
				
				else if (currentLevel == 7) {		
						System.out.println("You win");
				}
				
				else {
					for (int x = currentLevel - 1; x <= currentLevel + 1; x++) {	
						if (Arrays.equals(levels[currentLevel].trampos[x].coord, virus.coord)) {
							switchLevel(levels[currentLevel], levels[x]);
							this.currentLevel = x;
							return;
				}}}
		}
		
		
		if (difficulty > 0) {		//NORMAL AND DIFFICULT

			 if (currentLevel == 7) {		
				System.out.println("You win");
				levels[currentLevel].grannys[0].valueGrannys = 1000000;
			 }
				
				else {
					for (int x = 0; x <= currentLevel + 1; x++) {
						if (Arrays.equals(levels[currentLevel].trampos[x].coord, virus.coord)) {
							switchLevel(levels[currentLevel], levels[x]);
							currentLevel = x;
							return;
						
				}}}

		}
		
		setTextLevel(currentLevel + 1);}
		
			

	
	public void setTextLevel(int level) {
		if (level > 0) {
		levelLabel.setText("Lvl " + level + ": "+ continent[level-1]);}		
		else {
		levelLabel.setText("Homescreen");
		}
	}
	
	
	public void switchLevel(Board from, Board to) { // change FROM one board TO the other board
		bdw = to.bdw;
		bdh = to.bdh;
		updateRIB();
		resizeVirus(virus);
		virus.coord = toolbox.randomCoord(1,bdh);

		from.active = false;
		mainPanel.switchBoardPanel(to);
		repaint();
		revalidate();
	}

	/**
	 * Every time the virus kills a granny, he wins 2sec of time and a certain amount of points
	 * depending on the level
	 * 
	 * @param perso : your character, the virus 
	 * @param grannys : granny's location
	 */
	
	public void killGranny(Virusperso perso, SpecialTile[] grannys) {
		for (int i = 0; i < grannys.length; i++) {
			
		if (Arrays.equals(grannys[i].coord, perso.coord) && grannys[i].isVisible) {
				score += grannys[i].valueGrannys;
				grannys[i].isVisible = false;
				grannys[i].valueGrannys = 0;
				
				if (timeBarPaneel.currentTime <= 58 ) {
					timeBarPaneel.changeWidth(1);
				} 
			}
		}
		scoreLabel.setText("SCORE: " + score);
	}	


	public void updateVirus() {
		
		
		gainLife(virus, levels[currentLevel].vaccins);
		loseLife(virus, levels[currentLevel].doctors);
		killGranny(virus, levels[currentLevel].grannys);
		if(virus.life <= 0) {
			updateTimer.stop();
			timeBarPaneel.currentTime = 0;
		}
	}
	
	public void resetVirus() { 	
		for(int i=0; i <3; i++) {
		this.virus.heartVisible(this.virus.hearts[i], true);}
		timeBarPaneel.currentTime = 60;	
		score = 0;
	}
	
	public void resizeVirus(Virusperso virus) {
		virus.RIB = RIB;
		if(levels[currentLevel] != null) {
			virus.image = levels[currentLevel].virusImage;
		}
	}
	
	
	/*
	 * gainLife gives an extra life to the virus and teleports it on a random tile
	 * with x coordinates being 0
	 * loseLife does the same but the virus loses life
	*/
	
	public void loseLife(Virusperso perso, Doctor[] docs) {
		for(int i = 0; i < docs.length; i++) {
			if (Arrays.equals(docs[i].coord , perso.coord)) {
				if(perso.life - 1 >= 0) 
					{perso.life -= 1;}
				perso.heartVisible(perso.hearts[perso.life], false);
				perso.coord = toolbox.randomCoord(1,bdh);
		}}
	}

	
	public void gainLife(Virusperso perso, SpecialTile[] vaccins) {
		for(int i = 0; i < vaccins.length; i++) {
			if(Arrays.equals(vaccins[i].coord, perso.coord)) {
				 if(toolbox.rng(2) == 0) { //the vaccine has 1/2 chance to give a life back
					 if(perso.life + 1 < 4) { 
						 perso.life += 1;
						 perso.heartVisible(perso.hearts[perso.life - 1], true); 
					}	 	
				}
				 perso.coord = toolbox.randomCoord(1,bdh);
			}
		}
	}
	
	
	public void updateRIB(){ 
		RIB = (int) (Interface.framewidth / (double) bdw);
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == updateTimer) {
			levelChangeCheck();
			updateVirus();
			levels[currentLevel].repaint();
		}
	}
}


