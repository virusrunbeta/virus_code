package virusself;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class TimeBar extends JPanel {

	private int originalWidth, currentWidth;
	public int currentTime = 60;

	public TimeBar() {
		originalWidth = 306;
		currentWidth = 300;
	}

	@Override
	public void paintComponent(Graphics g) {

		g.setColor(Color.black);					//To cover up a bug on windows
	    g.fillRect(0, 0, getWidth(), getHeight());
		g.setColor(new Color(220, 220, 220));
		g.fillRoundRect(0, 5, originalWidth, 20, 10, 10);
		g.setColor(new Color(220,20,60));
		g.fillRoundRect(3, 8, currentWidth, 14, 10, 10);
		
	}

	public void changeWidth(int deltaTime) {
		currentTime += deltaTime;
		currentWidth = (originalWidth - 6) / 60 * currentTime;
	}	
	
	public void setCurrentTimeZero() {
		currentTime = 1;
	}
}