package virusself;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.ImageObserver;

import javax.swing.ImageIcon;
import javax.swing.JLabel;


public class Virusperso extends Tile implements ImageObserver {

	public int life = 3;
	public Image image;
	public JLabel[] hearts = new JLabel[3];

	public Virusperso(int x, int y) {
		super(x,y);
		
		int spacing = 70;	//space between the hearts being 70px
		for(int i =0; i<3; i++) {
			hearts[i] = new JLabel();
			CustomComponent.customJLabel(hearts[i], Color.red, 870 + i*spacing, 12, 300, 30, 30);
			hearts[i].setIcon(new ImageIcon(CustomComponent.customImage(getClass().getResource("/images/heart.png"),33,33)));
		}
		
	}
	

	public void draw(Graphics g, Image image) {
		g.drawImage(image,(int)(coord[0]*RIB + 2*PLAATS)  ,(int)(coord[1]*RIB + 2*PLAATS), (ImageObserver) this);
	}

	public void heartVisible(JLabel heart, boolean visible) {
		heart.setVisible(visible);
	}
	
	@Override
	public boolean imageUpdate(Image img, int infoflags, int x, int y, int width, int height) {
		// TODO Auto-generated method stub
		return false;
	}

}
