package virusself;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class HomeScreen extends JPanel {
	
	private Tools toolbox = new Tools();
	private JButton easyButton, normalButton, hardButton, tutorialButton;
	private JLabel virusTitle;

	private Image virusImage = new ImageIcon(CustomComponent.customImage(getClass().getResource("/images/virus.png"),100,100)).getImage();
		
	public HomeScreen(Interface mainPanel) {

		this.setBackground(toolbox.cherryPink);
		this.setLayout(null);

		
		easyButton = new JButton("EASY");
		CustomComponent.customJButton(easyButton,toolbox.paleBlue,235,370,150,50,14);	//Method for making JButtons more easily
		add(easyButton);
		easyButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				mainPanel.gameWindow.gamePlanMaker(mainPanel, 0);
				mainPanel.gameTimer.start();
			}
		});

		normalButton = new JButton("NORMAL");
		CustomComponent.customJButton(normalButton, toolbox.paleBlue, 565,370,150,50,14);
		add(normalButton);
		normalButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				mainPanel.gameWindow.gamePlanMaker(mainPanel, 1);
				mainPanel.gameTimer.start();
			}
		});
		
		
		hardButton = new JButton("HARD");
		CustomComponent.customJButton(hardButton, toolbox.paleBlue, 895,370,150,50,14);
		add(hardButton);
		hardButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				mainPanel.gameWindow.gamePlanMaker(mainPanel, 2);
				mainPanel.gameTimer.start();
			}
		});

		tutorialButton = new JButton("TUTORIAL");
		CustomComponent.customJButton(tutorialButton, toolbox.paleBlue, 580,550,120,50,14);
		add(tutorialButton);
		tutorialButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				mainPanel.switchPanel(mainPanel.tutorialPanel);
			}

		});

		virusTitle = new JLabel("VirusRun");
		CustomComponent.customJLabel(virusTitle, new Color(255,25,140), 470, 110, 500, 80, 70);
		add(virusTitle);
		
	}
	
	public void paintComponent(Graphics g) {
		
		super.paintComponent(g);

		//We made 3 different place for the virusImage to spawn so that it wouldn't be on the title

		int y = 4;
		while(y >0) {
		g.drawImage(virusImage, toolbox.rndg(10,370), toolbox.rndg(10, 600), null);
		y -=1;
		}
		int x = 4;
		while(x >0) {
		g.drawImage(virusImage, toolbox.rndg(470,870), toolbox.rndg(190, 600), null);
		x -=1;
		}
		int z = 4;
		while(z >0) {
		g.drawImage(virusImage, toolbox.rndg(970,1180), toolbox.rndg(10, 600), null);
		z -=1;
		}

	}
}
