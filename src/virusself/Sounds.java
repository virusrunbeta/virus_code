package virusself;


import java.net.URL;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;


public class Sounds {

    private URL activeMusic;
    private Clip activeClip;

    Sounds() {

        try {
            activeMusic = getClass().getResource("/musics/song2.wav");
            AudioInputStream as = AudioSystem.getAudioInputStream(activeMusic);
            this.activeClip  = AudioSystem.getClip(); //Using activeClip as the Clip we want to interact with
            activeClip.open(as); 					  //opening the clip before interacting with it
            activeClip.start(); 

            activeClip.loop(Clip.LOOP_CONTINUOUSLY);

            }
        catch(Exception e) {
        e.printStackTrace();
        }
    }


    //switching between different music files
    void switchMusic(String Play) { 

        try {

        activeClip.stop(); //stops playing music from the active clip
        this.activeMusic = getClass().getResource(Play);
        AudioInputStream as = AudioSystem.getAudioInputStream(activeMusic);

        this.activeClip  = AudioSystem.getClip();
        activeClip.open(as);
        activeClip.start(); 

        activeClip.loop(Clip.LOOP_CONTINUOUSLY);
    }
        catch(Exception e) {
            e.printStackTrace();
            }
}
}