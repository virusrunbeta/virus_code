package virusself;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

@SuppressWarnings("serial")
public class Board extends JPanel implements KeyListener, ActionListener {

	
//--attributen--//

	private Tools toolbox = new Tools();
	
	public int bdw, bdh; 
	public int grannyKillValue;
	public int level;
	private int RIB;
	
	public Tile[][] board, masking;
	private Virusperso virus;
	
	public Doctor[] doctors;
	public SpecialTile[] grannys, vaccins, trampos;
	
	public 	Boolean mask;						// says if there if the game starts black or not
	public  Boolean active = false;			//tells if this is the board showing on the screen
	
	private Timer doctorTimer; 					//refresh speed of the doctors

	private int xchange = 0, ychange = 0; 		// verandering in x en y richting, gebruikt bij het verplaatsen van het virus
	private int dsx = 1;                        //changing speed of the doctor in x and y
	private int dsy = 1;
	
	private Image trampoImage = new ImageIcon(getClass().getResource("/images/trampoline.png")).getImage();
	private Image grandmaImage = new ImageIcon(getClass().getResource("/images/grandma.png")).getImage();
	private Image vaccinImage = new ImageIcon(getClass().getResource("/images/vaccine.png")).getImage();
	private Image doctorImage = new ImageIcon(getClass().getResource("/images/doctor.png")).getImage();
	public Image virusImage = new ImageIcon(getClass().getResource("/images/virus.png")).getImage();

	
	
//--methodes--//

	// (bdw, bdh, #docs, #trampos, #grans, #vaccines, masker on or of, virus, level, grannyValue, doctorSpeed)
	public Board(int bdw, int bdh, int docs, int tramposs, int grans, int vaccinss, boolean mask, Virusperso virus,
			int level, int grannyValue, int docSpeed) { 

		RIB = (int) (Interface.framewidth / (double) bdw);
		this.virus = virus;
		
		this.bdw = bdw;
		this.bdh = bdh;
		this.level = level;
		this.board = new Tile[bdw][bdh];
		this.doctors = new Doctor[docs];
		this.grannys = new SpecialTile[grans];
		this.vaccins = new SpecialTile[vaccinss];
		this.masking = new Tile[bdw][bdh];
		this.trampos = new SpecialTile[tramposs];
		this.mask = mask;
		this.grannyKillValue = grannyValue;
		
		this.doctorTimer = new Timer(docSpeed, this); // refreshSpeed of the doctor
		doctorTimer.start();
			
		//resizing the images for this particular board
		this.trampoImage = new ImageIcon(trampoImage.getScaledInstance(RIB - 3*Tile.PLAATS , RIB - 3*Tile.PLAATS, Image.SCALE_DEFAULT)).getImage();
		this.grandmaImage = new ImageIcon(grandmaImage.getScaledInstance(RIB - 3*Tile.PLAATS , RIB - 3*Tile.PLAATS, Image.SCALE_DEFAULT)).getImage();
		this.vaccinImage = new ImageIcon(vaccinImage.getScaledInstance(RIB - 3*Tile.PLAATS , RIB - 3*Tile.PLAATS, Image.SCALE_DEFAULT)).getImage();
		this.doctorImage = new ImageIcon(doctorImage.getScaledInstance(RIB - 3*Tile.PLAATS , RIB - 3*Tile.PLAATS, Image.SCALE_DEFAULT)).getImage();
		this.virusImage = new ImageIcon(virusImage.getScaledInstance(RIB - 3*Tile.PLAATS , RIB - 3*Tile.PLAATS, Image.SCALE_DEFAULT)).getImage();
		//be giving a new image to the virus in each board, we don't have the problem of the virus' image becoming of bad quality because of resizing


		addKeyListener(this);
		setFocusable(true);
		setFocusTraversalKeysEnabled(false);

		for (int j = 0; j < bdh; j++) {
			for (int i = 0; i < bdw; i++) {
				board[i][j] = new Tile(i, j);
				board[i][j].tileColor = toolbox.kleuren(level);
				
				masking[i][j] = new SpecialTile(i, j);
				masking[i][j].tileColor = Color.BLACK;
			}
		}

	
		initialiseer(doctors);
		initialiseer(vaccins);
		initialiseer(trampos);
		initialiseer(grannys);
		
	}

	
	//Board tekenen
	public void drawBoard(Graphics g) { 

			g.setColor(Color.black);
			g.fillRect(0, 0, Interface.framewidth, Interface.frameheight); // achtergrond


			for (int i = 0; i < bdw; i++) {    // draw the tiles
				for (int j = 0; j < bdh; j++) {
					board[i][j].draw(g, RIB);
				}
			}

			for (int i = 0; i < grannys.length; i++) { //drawing of the special tiles
				if (grannys[i].isVisible) {
					grannys[i].draw(g, grandmaImage);
				}
			}
			for (int i = 0; i < vaccins.length; i++) { 											
				if (vaccins[i].isVisible) {
					vaccins[i].draw(g, vaccinImage);
				}
			}
			for (int i = 0; i < trampos.length; i++) {
				trampos[i].draw(g, trampoImage);
			}

			for (int i = 0; i < doctors.length; i++) { // drawing of the doctors
				doctors[i].draw(g, doctorImage);
			}


			if (virus.isVisible) {
				virus.draw(g, virusImage);	//drawing of the virus
			}

			//draws the black foreground 
			//inspired by WPO game of life
			if (mask) {

				for (int nx = virus.coord[0] - 1; nx <= virus.coord[0] + 1; nx++) {
					if (nx < 0 || nx >= masking.length) {
						continue;
					}

					 if (masking[nx][virus.coord[1]].isVisible) {
						masking[nx][virus.coord[1]].isVisible = false;
					}
				}

				for (int ny = virus.coord[1] - 1; ny <= virus.coord[1] + 1; ny++) {
					if (ny < 0 || ny >= masking[0].length) {
						continue;
					}

					if (masking[virus.coord[0]][ny].isVisible) {
						masking[virus.coord[0]][ny].isVisible = false;
					}
				}

				g.setColor(Color.black);
				for (int i = 0; i < bdw; i++) {
					for (int j = 0; j < bdh; j++) {
						if (masking[i][j].isVisible) {
							masking[i][j].draw(g,RIB);
						}
					}
				}

			}
		}
	
	// ----- update methods ------//
	
	public void updateDoctor() { 

        //this method can be written in less code but for sake of readability we wrote out every case

            for (int i = 0; i < doctors.length; i++) {

                int xd = doctors[i].coord[0]; // x coordinate doctor
                int yd = doctors[i].coord[1];

                int xp = virus.coord[0];       // x coordinate virus
                int yp = virus.coord[1];

                //if statement to limit the board
                if (-1 < (xd + dsx) && -1 < (yd + dsy) && (yd + dsy) < bdh && (xd + dsx) < bdw){ 

                    if (toolbox.rng(3) > 0) { //the doctor has 2/3 chance to move

                        //this makes the doctor move depending on the position of the virus
                        if (xd - xp == 0) {
                            dsx = 0;
                        } else if (xd - xp < 0) {
                            dsx = doctors[i].speedx;
                        } else if (xd - xp > 0) {
                            dsx = -doctors[i].speedx;
                        }
                        if (yd - yp == 0) {
                            dsy = 0;
                        } else if (yd - yp < 0) {
                            dsy = doctors[i].speedy;
                        } else if (yd - yp > 0) {
                            dsy = -doctors[i].speedy;
                        }
                    }

                    doctors[i].coord[0] = xd + dsx;
                    doctors[i].coord[1] = yd + dsy;
                    dsx = 0;
                    dsy = 0;
                }
            }
        }	
	
	// methods to make the virus move

	public void up() {
		xchange = 0;
		ychange = -1;
	}

	public void down() {
		xchange = 0;
		ychange = 1;
	}

	public void right() {
		xchange = 1;
		ychange = 0;
	}

	public void left() {
		xchange = -1;
		ychange = 0;
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int code = e.getKeyCode();
		if (code == KeyEvent.VK_UP) {
			up();
		}
		if (code == KeyEvent.VK_DOWN) {
			down();
		}
		if (code == KeyEvent.VK_RIGHT) {
			right();
		}
		if (code == KeyEvent.VK_LEFT) {
			left();
		}
		
		if(active) {
	        //only update the virus coordinates when pressing a key 
	        if (-1 < (virus.coord[1] + ychange) && -1 < (virus.coord[0] + xchange) && (virus.coord[1] + ychange) < bdh && (virus.coord[0] + xchange) < bdw) { // om het spel te limiteren
	            virus.coord[1] += ychange;
	            virus.coord[0] += xchange;
	        }
	        ychange = 0;
	        xchange = 0;
	        }
	    }
		

	@Override
	public void actionPerformed(ActionEvent e) {
		if (active) {
			if (e.getSource() == doctorTimer) {			
				this.updateDoctor();
			}
		}	
	}


	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		this.drawBoard(g);
		}
		

	void initialiseer(Doctor[] doctors) {
		for (int i = 0; i < doctors.length; i++) {
			doctors[i] = new Doctor(1,1); 				// initialiseren van de array
			doctors[i].coord[0] = toolbox.rndg(1,bdw-1); //bdw-1 make that the doctor doesn't spawn on the edge of the board
			doctors[i].RIB = this.RIB; 
		}
	}

	void initialiseer(SpecialTile[] array) {
		
		if(array == vaccins) {
			for (int i = 0; i < array.length; i++) {
				int[] newCoord = virginTile(true);		//rndg(1,bdw) used so that there doesn't appear anything on the spwanplace of the virus
				array[i] = new SpecialTile(newCoord[0], newCoord[1]);
				array[i].RIB = this.RIB; 
			}}
		
		 else { 
			for (int i = 0; i < array.length; i++) {
				int[] newCoord = virginTile(false);		
				array[i] = new SpecialTile(newCoord[0], newCoord[1], grannyKillValue);
				array[i].coord = newCoord;
				array[i].RIB = this.RIB; 
			}
		}
	}

	

	public int[] virginTile(boolean vaccin) {	//used so that there wouldn't be 2 specialTiles on the same tile
		
		int[] newCoord = new int[2];
		
		if(vaccin) {		
		newCoord[0] = toolbox.rndg(2*bdw/3,bdw);	//we want vaccins to appear on the right side of the board
		newCoord[1] = toolbox.rng(bdh);
		}
	
		else {
			newCoord[1] = toolbox.rng(bdh);	newCoord[0] = toolbox.rng(bdw);
			}
		
		if (!board[newCoord[0]][newCoord[1]].bezet) { //if the tile isn't already used
			board[newCoord[0]][newCoord[1]].bezet = true;
			return newCoord;	
		}
		else {		
			return virginTile(vaccin);
			
		}
	}
	
	
	//needed when implementing keyListenener
	public void keyTyped(KeyEvent e) {}
	public void keyReleased(KeyEvent e) {}
	

}
