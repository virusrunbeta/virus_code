Contributors:

Burta Olivier, Jacquart Aline, Wyverkens Victor.
This game was made in the context of our Informatics course at first Bachelor Civil Engineering at VUB.
How to run the program : You can launch the game by opening it in your IDE and running the main method located in the Interface class.

Git : https://alinejcqrt@bitbucket.org/virusrunbeta/virus_code.git if any problems with connecting to the git occur, contact Aline at aline.jacquart@vub.be

Extern aid and resources

- The implementation of music in the game 

Our Sounds class is inspired by the video: How to play music in Java (Simple). Published on January 24th 2018, 
by Max O'Didily, from https://www.youtube.com/watch?v=3q4f6I5zi2w.

- The implementation of images in the game

The way we implemented images is inspired by our teacher's and assitants' documentation http://parallel.vub.ac.be/


- Music :
Composed by Olivier Burta

- The used images:

virus
<div>Icons made by <a href="https://www.flaticon.com/authors/vitaly-gorbachev" title="Vitaly Gorbachev">Vitaly Gorbachev</a> from
<a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a><div>

granny
<div>Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from
<a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>

trampoline

heart
<div>Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from
<a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>

doctor
<div>Icons made by <a href="https://www.flaticon.com/authors/itim2101" title="itim2101">itim2101</a> from
<a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>

vaccine
<div>Icons made by <a href="https://www.flaticon.com/authors/monkik" title="monkik">monkik</a> from
<a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>

game_over
Image par Ryan Morrison de Pixabay 
https://pixabay.com/fr/

Special thanks to Neil Van Acoleyen for helping us out in our darkest moments and the assistants Tobias
and Colas for answering our questions. 



